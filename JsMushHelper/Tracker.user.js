﻿// ==UserScript==
// @name         Tracker
// @version      0.1
// @description  Tracker
// @author       Amadare
// @match      http://mush.vg/*
// @match      http://mush.vg/#
// @match      http://mush.twinoid.com/*
// @match      http://mush.twinoid.com/#
// @match      http://mush.twinoid.es/*
// @match      http://mush.twinoid.es/#
// @grant      GM_xmlhttpRequest
// @require    http://code.jquery.com/jquery-latest.js
// ==/UserScript==
/* jshint -W097 */
'use strict';

var Main;
var isDebug = false;

var logManager;
var tracker = {
    logManager: null,
    parsePage: null,
    pageParser: null,
    options: null,
    notificationContext: null,
    find: function () { debugger; },
    reset: function () {
        this.logManager.reset();
        this.notificationContext.resetReportedMessages();
    }
}
 
var StandardEntryTypes = {
    moving: "moving",
    item: "item",
    sleep: "sleep",
    wake: "wake",
    death: "death",
    reading: "reading",
    cooking: "cooking",

    infection: "infection",
    slimetrap: "slimetrap",
    spore: "spore",
    trapper: "trapper",
    mushification: "mushification"
};
var PrivacyLevels = {
    Undefined: -1,
    Public: 0,
    Secret: 1,
    Covert: 2,
    Mush: 3,
    Private: 4,
    Revealed: 5
}

var StandardLogPatterns = [
    //Tracker movement
    {
        pattern: /(.*) left the room in the direction of the (.*)\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.moving,
                direction: "to",
                character: match[1],
                location: match[2]
            }
        }
    },
    {
        pattern: /(.*) entered the room from the (.*)\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.moving,
                direction: "from",
                character: match[1],
                location: match[2]
            }
        }
    },
    //Default movement
    {
        pattern: /(.*) entered the room\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.moving,
                direction: "from",
                character: match[1]
            }
        }
    },
    {
        pattern: /(.*) left the room\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.moving,
                direction: "from",
                character: match[1]
            }
        }
    },
    //Moving items
    {
        pattern: /(.*) has picked up the (.*)\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.item,
                action: "pick up",
                character: match[1],
                item: match[2]
            }
        }
    },
    {
        pattern: /(.*) has dropped the (.*)\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.item,
                action: "drop",
                character: match[1],
                item: match[2]
            }
        }
    },
    //sleep
    {
        pattern: /(.*) is lying down\.\.\. It's good to just stop and take a break. Destination : the land of dreams!\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.sleep,
                character: match[1]
            }
        }
    },
    {
        pattern: /(.*) wakes up calmly\.\.\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.wake,
                character: match[1],
                cause: match[2]
            }
        }
    },
    //death
    {
        pattern: /(.*) is dead\. Cause: (.*)/g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.death,
                character: match[1],
                cause: match[2]
            }
        }
    },
    //reading
    {
        pattern: /(.*) starts reading enthusiastically\.\.\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.reading,
                character: match[1]
            }
        }
    },
    //cooking
    {
        pattern: /(.*) cooked a little something for later on.+/g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.cooking,
                character: match[1]
            }
        }
    },

    //------------------------MUSH actions------------------------
    //infection
    {
        pattern: /(.*) approached (.*) in a predatory manner\.\.\. and spiked them discretely!/g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.infection,
                mush: match[1],
                victim: match[2]
            }
        }
    },
    //Slimetrap
    {
        pattern: /(.*) approached (.*) in a predatory manner\.\.\. and spiked them discretely!/g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.slimetrap,
                mush: match[1],
                victim: match[2]
            }
        }
    },
    //Spore
    {
        pattern: /(.*) has removed a spore\. It's truly repulsive\. And the smell\.\.\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.spore,
                mush: match[1]
            }
        }
    },
    //trap
    {
        pattern: /(.*) seemed to have suspiciously slipped something onto (.*)\./g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.trapper,
                mush: match[1],
                object: match[2]
            }
        }
    },
    //mushification
    {
        pattern: /(.*) suddenly stiffens... It looks like twisted yellow threads are escaping from their spacesuit and trying to penetrate the hull.../g,
        createInfo: function (entry) {
            var match = this.pattern.exec(entry);
            if (!match) return null;
            return {
                type: StandardEntryTypes.mushification,
                mush: match[1]
            }
        }
    },
];
var MushChannelActionPatters = [
    //Touch a booby-trapped unit
    {
        pattern: /<strong>(.*)<\/strong> has been infected by rummaging in a shelving unit which had been booby-trapped by <strong>(.*)<\/strong>\. Their contamination level is currently (.*)\./g,
        createInfo: function(entry) {
            var match = pattern.exec(entry);
            if (!match) return null;
            return {
                type: "trap", //todo: find place for mush channel action types
                infected: match[1],
                infector: match[2],
                level: match[3]
            }
        }
    }
];

//static classes
var Const = {
    PageReloadCheckInterval: 1000,
    AutoUpdateInterval: 30000,
    MaxMessagexInNotification: 5
}

var Icons = {
    DownTabOn: isDebug ? "img/down_tabon.png" : "https://raw.githubusercontent.com/amadare42/JsMushHelper/master/JsMushHelper/img/down_tabon.png",
    DownTabOff: isDebug ? "img/down_taboff.png" : "https://raw.githubusercontent.com/amadare42/JsMushHelper/master/JsMushHelper/img/down_taboff.png",
    Settings: isDebug ? "http://mush.twinoid.com/img/icons/ui/talkie.png" : "/img/icons/ui/pa_comp.png",
    AutoUpdateOff: "http://www.alphabounce.com/img/forum/icon_s_fuel.gif",
    AutoUpdateOn: "http://www.alphabounce.com/img/forum/icon_s_fuel_free.gif",
    Logo: "http://data.mush.twinoid.com/img/design/logo.png",
    RoomEvent: "http://mush.twinoid.com/img/icons/ui/door.png",
    ChatMessage: "http://mush.twinoid.com/img/icons/ui/talkie.png"
}

var NotifySelectorsSet = {
    Moving: new function () {
        this.isImportant = function (e) {
            return e.info.type === "moving";
        }
    },
    Everything: new function () {
        this.isImportant = function (e) {
            return true;
        }
    }
}

var UI = new function () {

    //private
    var permissionGranted;
    var supported = true;

    var createNotification = function (header, options) {
        if (isDebug) {
            console.log("Notification: " + options.body);
        }
        if (!permissionGranted)
            return;
        var n = new Notification(header, options);
        n.onclick = function () {
            window.focus();
            this.cancel();
        };
    }

    //public

    //methods

    this.GetNotificationsPermission = function () {
        if (!("Notification" in window)) {
            supported = false;
            return;
        }
        if (Notification.permission !== "denied") {
            Notification.requestPermission(function (permission) {
                if (permission === "granted") {
                    permissionGranted = true;
                }
            });
        }
    }

    this.SimpleNotify = function (message) {
        createNotification("Mush", {
            body: message,
            icon: Icons.Logo
        });
    }

    this.Notify = function (header, body, icon) {
        var options = {
            body: body,
            icon: icon,
            isClickable: true
        }
        createNotification(header, options);
    }
    this.NotifyWithButtons = function (header, body, icon, buttons) {
        var options = {
            body: body,
            icon: icon,
            buttons: buttons
        }
    }
}

var PageState = new function () {

    this.IsChatInputFocused = function () {
        return $('.chatboxfocus').length > 0;
    },

    this.Active = function () {
        return !document.hidden;
    }
}

//#region Classes

function Room(name) {
    this.name = name;
    this.cycles = [];
}

function Cycle(name) {
    this.name = name;
    this.entries = [];
}
Cycle.prototype.toString = function () {
    return "Cycle" + this.name + " (" + this.entries.length + ") ";
}

function Entry(id, time, text, info, privacyLevel) {
    this.id = id;
    this.time = time;
    this.text = text;
    this.info = info;
    this.privacyLevel = privacyLevel;
}

function ChatEntry(id, time, isMain, text, autor, threadId) {
    this.time = time;
    this.isMain = isMain;
    this.text = text;
    this.autor = autor;
    this.threadId = threadId;
    this.id = id;
}

function Options() {
    this.AutoUpdate = !isDebug;

    this.SetDefaults = function () {
        this.AutoUpdate = false;
    }
}

//runtime storage for room logs & interface of working with it
function LogManager() {

    this.cycle = null;
    this.currentRoom = null;
    this.rooms = [];

    this.setCycle = function (name) {
        if (!this.currentRoom)
            throw "No room was selected.";

        var cycle = false;
        for (var i = 0; i < this.currentRoom.cycles.length; i++) {
            if (this.currentRoom.cycles[i].name === name) {
                cycle = this.currentRoom.cycles[i];
                break;
            }
        }

        if (!cycle) {
            cycle = new Cycle(name);
            this.currentRoom.cycles.push(cycle);
        }

        this.cycle = cycle;
    };

    this.setRoom = function (name) {
        var room = false;
        for (var i = 0; i < this.rooms.length; i++) {
            if (this.rooms[i].name === name) {
                room = this.rooms[i];
                break;
            }
        }

        if (!room) {
            room = new Room(name);
            this.rooms.push(room);
        }
        this.cycle = null;
        this.currentRoom = room;
    };

    this.addLog = function (entry) {
        if (!this.cycle) {
            throw "Trying to add log entry to empty cycle.";
        }

        var exists = false;
        for (var i = 0; i < this.cycle.entries.length; i++) {
            if (this.cycle.entries[i].id === entry.id) {
                exists = true;
                break;
            }
        }
        if (exists) {
            return false;
        }

        this.cycle.entries.push(entry);
        return true;
    };

    this.reset = function () {
        this.cycle = null;
        this.currentRoom = null;
        this.rooms = [];
    }

    this.getMovements = function (charName) {
        var moves = [];
        for (var c = 0; c < this.currentRoom.cycles.length; c++) {
            for (var e = 0; e < this.currentRoom.cycles[c].entries.length; e++) {
                var entry = this.currentRoom.cycles[c].entries[e];
                if (entry.info.type === "moving" && entry.info.character === charName) {
                    moves.push(entry);
                }
            }
        }
        return moves;
    }
}

function NotificationContext(notifyRoomLogs, notifyMessages) {
    this.reportedMessages = [];
    this.roomLogSelectors = [];

    this.notifyAny = true;
    this.notifyMessages = notifyMessages;
    this.notifyRoomLogs = notifyRoomLogs;

    this.reportMessage = function(message) {
        var present = !this.reportedMessages.every(function (element) {
            return !(message.id === element.id && message.threadId === element.threadId);
        });
        if (present)
            return false;
        this.reportedMessages.push(message);
        return true;
    }

    this.resetReportedMessages = function()
    {
        this.reportedMessages = [];
    }
}

//parser for single entries
function EntryParser(currentTime, currentCycle) {
    this.currentTime = currentTime;
    this.currentCycle = currentCycle;

    //public

    this.ParseLocalChannelEntry = function (e) {
        var rawEntryContent = e.find(".what_happened"); //assume it standard log message
        if (rawEntryContent.length > 0) {
            return this._parseStandardLogEntry(e);
        }
        rawEntryContent = e.find(".bubble_schodinger"); //assume it cat message
        if (rawEntryContent.length > 0) {
            return this._parseShrodingerLogEntry(e);
        }

        console.error("Unknown local channel entry: ", e);
        return null;
    }

    this.ParsePublicChatEntry = function (e) {
        var isMainMessage = e.hasClass("mainmessage");
        var neronTalks = e.find(".neron_talks");
        var isNeron = neronTalks.length !== 0;

        var html;
        if (isNeron) {
            html = neronTalks.clone().children().remove("div, span").end();
        } else {
            html = e.find(".bubble p");
        }
        var text = this._cleanHtml(html);

        var autor = e.find(".buddy").text();
        var time = this._parseTime(e.find(".ago").text());

        //getting id message from report button
        var threadId;
        var id;
        var reportOnclickText = $(e.find(".replybuttons .butmini")[isMainMessage ? 2 : 1]).attr("onclick");
        if (isMainMessage) {
            id = EntryParser.MessageIdPattern.exec(reportOnclickText)[1];
            threadId = id;
        } else {
            var match = EntryParser.ReplyIdPattern.exec(reportOnclickText);
            threadId = match[1];
            id = match[2];
        }

        return new ChatEntry(id, time, isMainMessage, text, autor, threadId);
    }

    this.ParsePrivateChatEntry = function (e) {
        //todo: implement private chat parsing
        throw "not implemented";
    }

    //inner

    this._parseStandardLogEntry = function (e) {
        var rawWhatHappened = e.find(".what_happened").clone();

        var id = e.attr("data-id");
        var time = this._parseTime(rawWhatHappened.find(".ago").text());
        rawWhatHappened.find('.ago, .clear').remove().end(); //remove unessesary classes
        var text = this._cleanHtml(rawWhatHappened);
        var info = this._parseInfo(text);
        var privacyLevel = this._getPrivacyLevel(e);

        return new Entry(id, time, text, info, privacyLevel);
    }

    this._parseShrodingerLogEntry = function (e) {
        var rawTalks = e.find(".talks").clone();

        var id = e.attr("data-id");
        var time = this._parseTime(rawTalks.find(".ago").text());
        rawTalks.find('.ago, .clear').remove().end(); //remove unessesary classes
        var text = this._cleanHtml(rawTalks);
        var info = { type: "cat" };

        return new Entry(id, time, text, info, PrivacyLevels.Public);
    }

    //helperts

    this._getPrivacyLevel = function(element) {
        if (element.hasClass("personnal"))
            return PrivacyLevels.Private;
        if (element.hasClass("backtracked"))
            return PrivacyLevels.Revealed;
        return PrivacyLevels.Undefined;
    }

    this._cleanHtml = function (html) {
        try {
            var self = this;
            //todo: replace images with proper description
            html.find("img").replaceWith(function() {
                return self.__iconReplacer(this);
            }).end();
        }
        catch (ex) {
            console.error(ex);
        }

        return html.text().replace(/\n|  |\t/g, "");
    }

    this._parseInfo = function (text) {
        var info;
        for (var i = 0; i < EntryParser.StandardLogPatterns.length; i++) {
            info = EntryParser.StandardLogPatterns[i].createInfo(text);
            if (info)
                return info;
        }

        return { type: "unknown" };
    }

    this._parseTime = function (timeLabel) {
        switch (timeLabel) {
            case "moments ago":
                return this.currentCycle + "(" + this.currentTime + ")";

                //todo: time parsing
            default:
                return this.currentCycle + "(" + this.currentTime + ") - " + timeLabel;
        }
    }

    this.__iconReplacer = function(element)
    {
        if (element.className === "") {
            return $(element).attr("alt");
        }
        if (element.className === "recent")
            return "";
        if (element.className === "paslot")
            return "ap";
        return element.className + " ";
    }
}

EntryParser.StandardLogPatterns = [];
EntryParser.MessageIdPattern = /.*signalWallMessage.([^']*)\'/;
EntryParser.ReplyIdPattern = /.*signalWallReply.([^\/]*)\/([^']*).*'/;

function AwayNotificationAccumulator(notificationContext) {
    this.entries = [];
    this.chatEntries = [];
    this.favChatEntries = [];
    this.notificationContext = notificationContext;

    this.addEntry = function (entry) {
        this.entries.push(entry);
    }
    this.addChatEntry = function (message) {
        if (this.notificationContext.reportMessage(message))
            this.chatEntries.push(message);
    }
    this.addFavChatEntry = function (message) {
        if (this.notificationContext.reportMessage(message))
            this.favChatEntries.push(message);
    }

    this.notifyChatMessages = function ()
    {
        if (this.chatEntries.length === 0) {
            return;
        }

        var message = "";
        for (var i = 0; i < this.chatEntries.length; i++) {
            var entry = this.chatEntries[i];
            //todo: chat message selectors
            message += entry.autor + " " + entry.text + "\r\n";
        }

        if (message !== "") {
            UI.Notify("Mush - " + this.chatEntries.length + " unreaded messages.", message, Icons.ChatMessage);
        }
    }
    this.notifyRoomLogs = function () {
        if (this.entries.length === 0)
            return;

        var message = "";
        var selectors = this.notificationContext.roomLogSelectors;
        var count = 0;

        for (var i = 0; i < this.entries.length; i++) {
            var important = false;
            for (var j = 0; j < selectors.length ; j++) {
                if (selectors[j].isImportant(this.entries[i])) {
                    important = true;
                    break;
                }
            }
            if (important) {
                if (count >= Const.MaxMessagexInNotification) {
                    message += "\r\n...";
                    break;
                }
                message += this.entries[i].text + "\r\n";
                count++;
            }
        }
        if (message !== "") {
            UI.Notify("Mush - " + this.entries.length + " new room events", message, Icons.RoomEvent);
        }
    }
}

//parser for whole page
function PageParser() {

    var self = this;

    //inner

    this._getCycle = function () {
        var cycle = $($.find('.cycletime')).text();
        return cycle.replace(/ |\n|-|\t/g, "").replace(/Day/, "D").replace(/Cycle/, "C"); // + "(" + time + ")";
    }

    this._getTime = function () {
        var time = $($.find('.cdTimeStamp')).text();
        return time;
    }

    this._getRoom = function () {
        return $($.find('#input')).attr('d_name');
    }

    this._getLocalChannel = function () {
        return $(document).find("#localChannel").find("div.cdChatPack");
    }

    //public

    this.parsePage = function (notificationContext) {
        var entry;
        var i;

        var currentTime = self._getTime();
        var currentCycle = self._getCycle();
        var currentRoom = self._getRoom();

        var entryParser = new EntryParser(currentTime, currentCycle);
        var notificationAccumulator = new AwayNotificationAccumulator(notificationContext);

        logManager.setRoom(currentRoom);

        //parse local channel
        var segments = self._getLocalChannel();
        for (i = 0; i < segments.size() ; i++) {
            var jsegment = $(segments[i]);
            var segmentCycle = jsegment.find(".day_cycle").find("div").text();
            logManager.setCycle(segmentCycle);

            var entries = jsegment.find(".cdContent").find(".privLog");

            for (var j = 0;j < entries.size() ; j++) {
                var e = $(entries[j]);
                entry = entryParser.ParseLocalChannelEntry(e);
                if (!entry)
                    continue;

                var notReaded = e.hasClass("not_read");
                var isNew = logManager.addLog(entry);

                if (notReaded && isNew) {
                    notificationAccumulator.addEntry(entry);
                }
            }
        }

        //parse main chat
        var mainChat = $(".cdStdWall").find(".not_read");
        for (i = 0; i < mainChat.length; i++) {
            entry = entryParser.ParsePublicChatEntry($(mainChat[i]));
            if (!entry)
                continue;

            notificationAccumulator.addChatEntry(entry);
        }

        //parse favorites
        var favorites = $(".cdFavWall").find(".not_read");
        for (i = 0; i < favorites.length; i++) {
            entry = entryParser.ParsePublicChatEntry($(favorites[i]));
            notificationAccumulator.addFavChatEntry(entry);
        }

        //todo: private channels parsing
        //todo: mush channel parsing
        //todo: resourses parsing

        //notifications
        notificationAccumulator.notifyRoomLogs();
        notificationAccumulator.notifyChatMessages();

        console.log("full page parsed");
    }

    this.parsePage_NoNotification = function () {
        self.parsePage(new NotificationContext());
    }

    this.parseChatMessages = function () {
        //todo: move from parsePage
    }
}

//#endregion

function AutoUpdateBtn_Click() {
    tracker.options.AutoUpdate = !tracker.options.AutoUpdate;
    $("#trackerAutoupdateBtn").attr("src", tracker.options.AutoUpdate ? Icons.AutoUpdateOn : Icons.AutoUpdateOff);
}

//Initialisation stuff
function BuildTrackerView() {
    if ($("#trackerParseButton").length > 0) return; //check if already inited

    var localChannel = $(document).find("#localChannel");
    var underChat = $('.butPlace');

    //parse button
    var parseBtn = $('<button/>',
        {
            text: "parse",
            id: 'trackerParseButton',
            click: tracker.pageParser.parsePage_NoNotification
        });
    localChannel.prepend(parseBtn);

    //autoupdate button
    if ($("#trackerAutoupdateBtn").length > 0) return; //check if already present
    var optionsBtn = $('<img/>',
    {
        id: 'trackerAutoupdateBtn',
        click: AutoUpdateBtn_Click,
        mouseover: function (e) {
            optionsBtn.css("background-image", "url(" + Icons.DownTabOn + ")");
            TrackerTabTip(e, null, "Toggle autoupdate");
        }
    });
    optionsBtn.css({
        float: "left",
        cursor: "pointer"
    });
    optionsBtn.attr("src", tracker.options.AutoUpdate ? Icons.AutoUpdateOn : Icons.AutoUpdateOff);
    optionsBtn.on("mouseout", function () {
        optionsBtn.css("background-image", "url(" + Icons.DownTabOff + ")");
        Main.hideTip();
    });
    //todo: make button clickable by Vimium and with proper design

    underChat.prepend(optionsBtn);
}

var ViewBuilder = new function() {
    this.checkView = function() {
        return $("#trackerParseButton").length > 0; //if 
    }
}

function TrackerTabTip(e, header, text) {
    header = header ? header : "Tracker";
    text = text ? text : "";
    Main.showTip(e.target,
			"<div class='tiptop' ><div class='tipbottom'><div class='tipbg'><div class='tipcontent'>" +
			("<h1>" + header + "</h1>") +
			("<p>" + text + "</p>") +
			"</div></div></div></div>"
	);
}

function Init() {
    EntryParser.StandardLogPatterns = StandardLogPatterns;

    Main.window.tracker = tracker;
    logManager = new LogManager();
    tracker.logManager = logManager;
    tracker.pageParser = new PageParser();

    tracker.options = new Options();
    tracker.options.SetDefaults(); //todo: load from saved data
    tracker.notificationContext = new NotificationContext(true, true);
    tracker.notificationContext.roomLogSelectors = [NotifySelectorsSet.Everything];

    //todo: inject refreshChat detector to re-parse after partial page reloading

    //debugish stuff
    tracker.parseWithNotifications = function () {
        //todo: make pageParser static
        tracker.pageParser.parsePage(tracker.notificationContext);
    }

    UI.GetNotificationsPermission();
    InitTimers();

    //now = $("#input").attr('now');
}

function InitTimers() {
    setInterval(function () {//recreate parse button if page is reloaded
        BuildTrackerView();
        //todo: re-parse page if full page resfersh was detected
    }, Const.PageReloadCheckInterval);

    setInterval(function () {//auto-update and parse logs
        if (PageState.IsChatInputFocused() || !tracker.options.AutoUpdate)
            return;

        Main.refreshChat();
        //todo: new cycle notification
        tracker.notificationContext.notifyAny = !PageState.Active(); //don't notify when page is active
        tracker.pageParser.parsePage(tracker.notificationContext);
    }, Const.AutoUpdateInterval);
        
}

//#region DEBUG

function CreateDebugButton(name, click) {
    var btn = document.createElement("button");
    btn.appendChild(document.createTextNode(name));
    btn.addEventListener("click", click);
    document.body.appendChild(btn);
}

function MainMock() {
    this.refreshChat = function () {
        console.log("refreshed Chat!");
    }
    this.showTip = function (e, text) {
        console.log("tip " + text, e);
    }
    this.hideTip = function () {
        console.log("tip hidden");
    }

    this.window = window;
}

function debugInit() {
    isDebug = true;
}

//#endregion

if (!document.body) { //during tests, body is loaded after script
    $(document).ready(function () {
        Main = new MainMock();
        Init();
        debugInit();
    });
} else {
    Main = unsafeWindow.Main;
    CreateDebugButton("ParseWithNotifications", function() { tracker.parseWithNotifications(); });
    CreateDebugButton("ResetAll", function() { tracker.reset(); });
    Init();
}